import java.util.Collection;
import java.util.Hashtable;
import java.util.Set;
import java.util.Enumeration;

public class HastableDemo {
    public static void main(String[] args) throws Exception {
        //Creating Hashtable for example
        Hashtable companies = new Hashtable();
        //Java Hashtable example to put object into Hashtable
        //put(key, value) is used to insert object into map
        companies.put("Google", "USA");
        companies.put("Nokia", "Finland");
        companies.put("Sony", "Japan");
        //Java Hashtable example to get object from Hashtable
        //get(key) is used to retrieve Object from Hastable
        companies.get("Nokia");

        //Hashtable containsKey Example
        //Use containsKey(Object) method to check if an Object exist as key in Hastable
        System.out.println("Does Hashtable contains Google as key : " + companies.containsKey("Google"));

        //Hashtable containsValue Example
        //just like containsKey(), containsValue returns true if
        //hastable contains specified object as value
        System.out.println("Does hastable contains Japan as value: " + companies.containsValue("Japan"));

        //Hastable enumeration Example
        //hastable.elements() return enumeration of all hashtable values
        Enumeration enumeration = companies.elements();

        while (enumeration.hasMoreElements()) {
            System.out.println("Hashtable values: " + enumeration.nextElement());
        }

        //HOw to check if Hastable is empty in Java
        //use isEmpty() method to check if hastable is empty
        System.out.println("Is Hashtable empty : " + companies.isEmpty());

        //How to find sie of Hashtable in Java
        //use hastable.size() method to find size of hastable
        System.out.println("Size of Hashtable in Java : " + companies.size());

        //How to get all values from Hashtable in Java
        //You can use keySet() method to get a set of all the keys of hastable

        Set hastableKeys = companies.keySet();

        //You can also get enumeration of all keys by using method keys()
        Enumeration hastableKeysEnum = companies.keys();

        //How to get all keys from Hashtable in Java
        //There are two ways to get all values from hash table first by using
        //Enumeration and second getting values ad collection

        Enumeration hashtableValuesEnum = companies.elements();

        Collection hashtableValues = companies.values();
        
        //Hashtable clear example by using clear()
        //we can reuse an existing hastable, it clears all mappings
        companies.clear();
    }
}
