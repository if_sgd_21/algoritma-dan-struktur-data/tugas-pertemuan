import java.util.Hashtable;
import java.util.Scanner;
public class KamusHash {
    public static void main(String[] args) {
        Hashtable<String, String> kamus = new Hashtable<String, String>();
        Scanner in = new Scanner(System.in);
        //Kamus istilah Informatika
        kamus.put("Aplikasi", "Program yang dirancang untuk memenuhi kebutuhan pengguna");
        kamus.put("Bahasa Pemrograman", "Bahasa yang digunakan untuk menulis program komputer");
        kamus.put("Compiler", "Program yang mengubah program yang ditulis dalam bahasa pemrograman menjadi bahasa mesin");
        kamus.put("Debugger", "Program yang digunakan untuk menemukan kesalahan dalam program");
        kamus.put("Hardware", "Komponen fisik komputer");
        kamus.put("Interpreter", "Program yang mengubah program yang ditulis dalam bahasa pemrograman menjadi bahasa mesin");
        kamus.put("Jaringan Komputer", "Kumpulan komputer yang terhubung satu sama lain");
        kamus.put("Komputer", "Alat yang dapat menerima data, memproses data, dan menghasilkan informasi");
        kamus.put("Komputer Pribadi", "Komputer yang digunakan oleh satu orang");
        kamus.put("Komputer Server", "Komputer yang digunakan untuk menyimpan data dan program");
        System.out.println("==========================================");
        System.out.println("Kamus istilah Informatika");
        System.out.println("==========================================");
        System.out.println("1. Tampilkan semua istilah");
        System.out.println("2. Cari istilah");
        System.out.println("3. Keluar");
        System.out.print("Pilihan Anda: ");
        int pilihan = in.nextInt();
        in.nextLine();
        while (pilihan != 3) {
            if (pilihan == 1) {
                System.out.println("Daftar istilah:");
                for (String istilah : kamus.keySet()) {
                    System.out.print(istilah + ", ");
                }
            } else if (pilihan == 2) {
                System.out.print("Masukkan istilah yang ingin Anda cari: ");
                String istilah = in.nextLine();
                if (kamus.containsKey(istilah)) {
                    System.out.println("Arti: " + kamus.get(istilah));
                } else {
                    System.out.println("Istilah tidak ditemukan");
                    //tambahkan istilah baru
                    System.out.print("Masukkan arti istilah baru: ");
                    String arti = in.nextLine();
                    kamus.put(istilah, arti);
                }
            } else {
                System.out.println("Pilihan tidak valid");
            }
            System.out.println();
            System.out.println("==========================================");
            System.out.println("Kamus istilah Informatika");
            System.out.println("==========================================");
            System.out.println("1. Tampilkan semua istilah");
            System.out.println("2. Cari istilah");
            System.out.println("3. Keluar");
            System.out.print("Pilihan Anda: ");
            pilihan = in.nextInt();
            in.nextLine();
        }
    }
}
