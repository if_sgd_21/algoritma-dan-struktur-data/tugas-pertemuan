public class MainBT {
    public static void main(String[] args) {
        BTNode tree = new BTNode(); 
        tree.add("Kakek");
        tree.add("Bapak");
        tree.add("Paman");
        tree.add("Saya");
        tree.add("Adik");
        tree.add("Andi");
        tree.add("Agung");
        //print
        System.out.println(tree.containsNode("Maman"));
        tree.print();
        System.out.println("Inorder: ");
        tree.inorder(tree.root);
        System.out.println();
        System.out.println("Preorder: ");
        tree.preorder(tree.root);
        System.out.println();
        System.out.println("Postorder: ");
        tree.postorder(tree.root);
    }
}
