public class BTString {
    Node root;
    //add method
    private Node addRecursive(Node current, String name) {
        if (current == null) {
            return new Node(name);
        }
        if (name.compareTo(current.name) < 0) {
            current.left = addRecursive(current.left, name);
        } else if (name.compareTo(current.name) > 0) {
            current.right = addRecursive(current.right, name);
        } else {
            // value already exists
            return current;
        }
        return current;
    }
    public void add(String name) {
        root = addRecursive(root, name);
    }
    //find method
    private boolean containsNodeRecursive(Node current, String name) {
        if (current == null) {
            return false;
        }
        if (name.compareTo(current.name) == 0) {
            return true;
        }
        return name.compareTo(current.name) < 0
          ? containsNodeRecursive(current.left, name)
          : containsNodeRecursive(current.right, name);
    }
    public boolean containsNode(String name) {
        return containsNodeRecursive(root, name);
    }
    //delete method
    private Node deleteRecursive (Node current, String name) {
        if (current == null) {
            return null;
        }
        if (name.compareTo(current.name) == 0) {
            // Node to delete found
            // ... code to delete the node will go here
            if (current.left == null && current.right == null) {
                return null;
            }
            if (current.right == null) {
                return current.left;
            }
            if (current.left == null) {
                return current.right;
            }
        }
        if (name.compareTo(current.name) < 0) {
            current.left = deleteRecursive(current.left, name);
            return current;
        }
        current.right = deleteRecursive(current.right, name);
        return current;
    }
    public void delete(String name) {
        root = deleteRecursive(root, name);
    }
    //print method
    private void printRecursive(Node current) {
        if (current != null) {
            printRecursive(current.left);
            System.out.println(current.name);
            printRecursive(current.right);
        }
    }
    public void print() {
        printRecursive(root);
    }
    //transversal method
    public void inorder (Node node) {
        if (node != null) {
            inorder(node.left);
            System.out.print(node.name + " ");
            inorder(node.right);
        }
    }
    public void preorder (Node node) {
        if (node != null) {
            System.out.print(node.name + " ");
            preorder(node.left);
            preorder(node.right);
        }
    }
    public void postorder (Node node) {
        if (node != null) {
            postorder(node.left);
            postorder(node.right);
            System.out.print(node.name + " ");
        }
    }
}