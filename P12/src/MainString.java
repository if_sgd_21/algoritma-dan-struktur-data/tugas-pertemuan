import java.util.Scanner;
public class MainString {
    public static void main(String[] args) {
        BTString tree = new BTString();
        System.out.println("Program Binary Search Tree");
        System.out.println("===================================");
        System.out.println("1. Input Nama");
        System.out.println("2. Cari Nama");
        System.out.println("3. Hapus Nama");
        System.out.println("4. Tampilkan Nama");
        System.out.println("5. Keluar");
        System.out.println("===================================");
        Scanner input = new Scanner(System.in);
        int pilih;
        do {
            System.out.print("Pilih Menu : ");
            pilih = input.nextInt();
            switch (pilih) {
                case 1:
                    System.out.print("Masukkan jumlah nama yang akan diinput: ");
                    int jumlah = input.nextInt();
                    for (int i = 0; i < jumlah; i++) {
                        System.out.print("Masukkan nama ke-" + (i + 1) + ": ");
                        String nama = input.next();
                        tree.add(nama);
                    }
                    break;
                case 2:
                    System.out.print("Cari Nama : ");
                    String cari = input.next();
                    System.out.println(tree.containsNode(cari));
                    break;
                case 3:
                    System.out.print("Hapus Nama : ");
                    String hapus = input.next();
                    tree.delete(hapus);
                    break;
                case 4:
                    System.out.println("Inorder: ");
                    tree.inorder(tree.root);
                    System.out.println();
                    System.out.println("Preorder: ");
                    tree.preorder(tree.root);
                    System.out.println();
                    System.out.println("Postorder: ");
                    tree.postorder(tree.root);
                    System.out.println();
                    break;
                case 5:
                    System.out.println("Keluar");
                    break;
                default:
                    System.out.println("Pilihan tidak ada");
            }
        } while (pilih != 5);
        input.close();
    }
}
