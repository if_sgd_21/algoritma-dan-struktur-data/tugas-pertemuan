class Node {
    int value;
    String name;
    Node left;
    Node right;

    Node (int value) {
        this.value = value;
        right = null;
        left = null;
    }

    Node (String name) {
        this.name = name;
        right = null;
        left = null;
    }

    Node (String name, int value) {
        this.value = value;
        this.name = name;
        right = null;
        left = null;
    }
}
