public class Main  {
    public static void main(String[] args) {
        BinaryTree tree = new BinaryTree();
        tree.add(5);
        tree.add(2);
        tree.add(4);
        tree.add(7);
        tree.add(1);
        tree.add(6);
        tree.add(3);
        tree.add(8);
        // Elemen yang dimasukkan
        // Elemen apa yang mau dicari? Jika ketemu, akan menampilkan true
        System.out.println(tree.containsNode(5));
        // Elemen apa yang mau dihapus?
        tree.delete(8);
        System.out.println("Inorder: ");
        tree.inorder(tree.root);
        System.out.println();
        System.out.println("Preorder: ");
        tree.preorder(tree.root);
        System.out.println();
        System.out.println("Postorder: ");
        tree.postorder(tree.root);
        System.out.println();
    }
}