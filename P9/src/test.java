public class test {
    public static void main(String[] args) {
        String name = "Dian";
        String anotherName = "Dian";
        //check the address of each
        System.out.println("name: " + System.identityHashCode(name)); 
        System.out.println("anotherName: " + System.identityHashCode(anotherName));
        //check the equality of each
        System.out.println("name == anotherName: " + (name == anotherName));
        System.out.println("name.equals(anotherName): " + name.equals(anotherName));
    }
}