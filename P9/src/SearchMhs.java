import java.util.Scanner;
public class SearchMhs {
    public static void main(String[] args) {
        int c, n;
        String nama[];
        String cari;
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan jumlah nama yang akan diinputkan : ");
        n = in.nextInt();
        nama = new String[n];
        System.out.println("Masukkan nama-nama yang akan diinputkan :");
        for (c = 0; c < n; c++)
            nama[c] = in.next();
        System.out.print("Masukkan nama yang akan dicari : ");
        cari = in.next();
        for (c = 0; c < n; c++) {
            if (nama[c].equals(cari)) {
                System.out.println(cari + " ditemukan pada indeks ke " + (c + 1) + ".");
                break;
            }
        }
        if (c == n)
            System.out.println(cari + " tidak ada dalam array.");
        in.close();
    }
}
