import java.util.*;

public class SortingMhs {
    public static void sort(String[] nama)
    {
        int inner, outer;
        String temp;
        int h = 1;
        while (h <= nama.length/3)
        {
            h = h * 3 + 1;
        }
        while (h>0)
        {
            for (outer = h; outer < nama.length; outer++)
            {
                temp = nama[outer];
                inner = outer;
                while (inner > h-1 && nama[inner-h].compareTo(temp) >= 0)
                {
                    nama[inner] = nama[inner-h];
                    inner -= h;
                }
                nama[inner] = temp;
            }
            h = (h-1)/3;
        }
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan jumlah nama yang akan diinput : ");
        int n = in.nextInt();
        String[] nama = new String[n];
        System.out.print("Masukkan nama-nama yang akan diinput : ");
        for (int i = 0; i < n; i++) {
            nama[i] = in.next();
        }
        System.out.println("Sebelum diurutkan : " + Arrays.toString(nama));
        sort(nama);
        System.out.println("Setelah diurutkan : " + Arrays.toString(nama));
        in.close();
    }
}