import java.util.Scanner;

public class tugas {
    public static void main(String[] args) {
        try(Scanner input = new Scanner(System.in)){
            int banyak, a;
            String tempnama, tempnim;
            System.out.print("Mahasiswa banyak data yang akan diinput: ");
            banyak = input.nextInt();
            String nama[] = new String[banyak];
            String nim[] = new String[banyak];
            for(;;){
                System.out.println("!!!#####====Main Menu====#####!!!");
                System.out.print("Pilihlah Salah Satu Menu Di Bawah\n1. Masukkan Data Mahasiswa\n2. Tampilkan Data Urut Berdasarkan Nama\n3. Tampilkan Data Urut Berdasarkan NIM\n4. Exit");
                System.out.println();
                a = input.nextInt();
                switch (a) {
                    case 1:
                        System.out.println("Masukkan Data Mahasiswa (Nama dan NIM)");
                        for(int x = 0; x < banyak; x++){
                            System.out.print("NIM Mahasiswa" + (x + 1) + " : ");
                            nim[x] = input.next();
                            System.out.print("Nama Mahasiswa" + (x + 1) + " : ");
                            nama[x] = input.next();
                        }
                        System.out.println();
                        break;
                    case 2:
                        System.out.println("Data Mahasiswa Berdasarkan Nama");
                        for(int x = 0; x < banyak; x++){
                            for(int y = 0; y < banyak; y++){
                                if(nama[x].compareTo(nama[y]) < 0){
                                    tempnama = nama[x];
                                    nama[x] = nama[y];
                                    nama[y] = tempnama;
                                    tempnim = nim[x];
                                    nim[x] = nim[y];
                                    nim[y] = tempnim;
                                }
                            }
                        }
                        for(int x = 0; x < banyak; x++){
                            System.out.println("Nama Mahasiswa" + (x + 1) + " : " + nama[x]);
                        }
                        System.out.println();
                        break;
                    case 3:
                        System.out.println("Data Mahasiswa Berdasarkan NIM");
                        for(int x = 0; x < banyak; x++){
                            for(int y = 0; y < banyak; y++){
                                if(nim[x].compareTo(nim[y]) < 0){
                                    tempnim = nim[x];
                                    nim[x] = nim[y];
                                    nim[y] = tempnim;
                                    tempnama = nama[x];
                                    nama[x] = nama[y];
                                    nama[y] = tempnama;
                                }
                            }
                        }
                        for(int x = 0; x < banyak; x++){
                            System.out.println("NIM Mahasiswa" + (x + 1) + " : " + nim[x]);
                        }
                        System.out.println();
                        break;
                    case 4:
                    System.out.println("!===========!KELUAR!===========!\nTerimakasih Atas Kunjungan Anda");
                    System.exit(0);
                    default:
                    System.out.println("Pilihan Tidak Ada Di Menu!!!");
                }
            }
        }
    }
}