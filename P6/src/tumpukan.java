import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack; //ini sih jelas buat stack
public class tumpukan {
    private static Stack<String> stack;
    private static int jumlah;
    public static void main(String[] args) {
        System.out.println("Program Stack Sederhana");
        System.out.println("Mengurut Daftar Barang");
        System.out.println("==================================");
        System.out.print("Masukkan jumlah barang yang terjual : ");
		jumlah=inputData();
		buatStack(jumlah);
		bacaData();
		tulisData();
		System.out.println("==================================");
		System.out.println("Terima kasih telah menggunakan program ini");
		System.out.println("==================================");
	}

	private static void buatStack(int jumlah)
	{
		stack=new Stack<String>();
	}
	
    private static void bacaData()
	{
		String data;
		System.out.println("Masukan daftar barang: ");
		for(int i=0;i<jumlah;i++)
		{
			System.out.print("Barang ke-"+(i+1)+" : ");
			data=inputStr();
			stack.push(data);
		}
	}
    private static void tulisData()
	{
		System.out.println("Isi Stack adalah (menggunakan prosedur pop)");
		for(int i=0;i<jumlah;i++)
		{
			System.out.println(stack.pop());
		}
	}
	private static Integer inputData()
	{
		BufferedReader bfr=new BufferedReader(new InputStreamReader(System.in));
		String angkaInput=null;
		try
		{
			angkaInput=bfr.readLine();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		int Data=Integer.valueOf(angkaInput).intValue();
		return Data;
    }
	private static String inputStr()
	{
		BufferedReader bfr=new BufferedReader(new InputStreamReader(System.in));
		String strInput=null;
		try
		{
			strInput=bfr.readLine();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return strInput;
	}
}
