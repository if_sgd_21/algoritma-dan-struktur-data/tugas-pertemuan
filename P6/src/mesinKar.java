import java.util.Scanner;

public class mesinKar {
    static void withSpace(String teks)
    {
        // ukuran jumlah karakter string dihitung
        // lalu dimasukkan sebagai jumlah array pada char
        int size = teks.length();
        // System.out.println(size); <== kalo kepo boleh diuncomment
        char[] CC = new char[size];
        //kenapa diubah jadi char? karena char lebih mudah diolah
        // daripada string, karena char bisa dipecah jadi satu per satu
        // sedangkan string tidak bisa
        // jadi string harus diubah ke char dulu
        // lalu diolah satu per satu biar kayak mesin karakter
        CC = teks.toCharArray(); //ini proses ubah string ke char
        for(int i = 0; i < size; i++) //pake perulangan biar bisa dicetak 1 per 1
        {
            //use delay time
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.print(CC[i]);
        }
    }
    static void withoutSpace(String teks)
    {
        int size = teks.length();
        System.out.println("Jumlah karakter (termasuk spasi): " + size);
        char[] CC = new char[size];
        CC = teks.toCharArray();
        //menghitung size teks tanpa spasi
        int size2 = 0;
        for(int i = 0; i < size; i++)
        { 
            //use delay time
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (CC[i] == ' ') //jika karakter ada spasi
            {
                size2++; //maka size2 ditambah 1
            }
            else //jika karakter tidak ada spasi
            {
                System.out.print(CC[i]); //maka langsung dicetak
            }
        }
        size2 = size - size2; //size awal dikurangi size2 yang berisi spasi
        System.out.print("\n");
        for (int i = 0; i < size2; i++) //pake perulangan biar bisa dicetak 1 per 1
        {
            //delay time
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.print((i+1) + " "); //dicetak dengan angka
        }
        System.out.println("\nJumlah karakter (tanpa spasi): " + size2);
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Program Mesin Karakter");
        System.out.print("Masukkan kalimat: ");
        String kalimat = in.nextLine();
        System.out.println("Kalimat yang dimasukkan: " + kalimat);
        System.out.println("Silahkan pilih menu dibawah ini:");
        System.out.println("1. Proses Pembacaan dan Penampilan Karakter Satu Per Satu");
        System.out.println("2. Proses penghitungan Karakter tanpa menghitung spasi");
        int pil = in.nextInt();
        switch (pil) {
            case 1:
                withSpace(kalimat);
                break;
            case 2:
                withoutSpace(kalimat);
                break;
            default:
                System.out.println("Input tidak valid");
                break;
        }
        in.close();
    }
}
