import java.util.Scanner;

public class prime {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan bilangan : ");
        int i = in.nextInt();
        in.close();
        isPrime(i);
    }
    private static boolean isPrime(int bil)
    {
        if (bil == 2)
        {
            System.out.println(bil + " adalah bilangan prima");
            return true;
        }
        else if (bil % 2 == 0)
        {
            System.out.println(bil + " bukan bilangan prima");
            return false;
        }
        else
        {
            for (int i = 3; i < bil; i += 2)
            {
                if (bil % i == 0)
                {
                    System.out.println(bil + " bukan bilangan prima");
                    return false;
                }
            }
            System.out.println(bil + " adalah bilangan prima");
            return true;
        }
    }
}
