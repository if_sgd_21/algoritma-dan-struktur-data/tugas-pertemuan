import java.util.Scanner;

public class primeRec {
    public static int prime(int n, int i) {
        if (i == 1) {
            return 1;
        } else {
            if (n % i == 0) {
                return 0;
            } else {
                return prime(n, i - 1);
            }
        }
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan bilangan : ");
        int n = in.nextInt();
        in.close();
        if (prime(n, n / 2) == 1) {
            System.out.println(n + " adalah bilangan prima");
        } else {
            System.out.println(n + " bukan bilangan prima");
        }
    }
}
