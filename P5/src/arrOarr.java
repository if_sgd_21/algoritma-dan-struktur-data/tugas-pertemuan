public class arrOarr {
    public static void main(String[] args) {
        int[][] arr = new int[6][];
        arr[0] = new int[] {};
        arr[1] = new int[] {1};
        arr[2] = new int[] {1, 2};
        arr[3] = new int[] {1, 2, 3};
        arr[4] = new int[] {1, 2, 3, 4};
        arr[5] = new int[] {1, 2, 3, 4, 5};
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
        
    }
}
