import java.util.Scanner;

public class reverseStr {
    static String reverse(String str) {
        if (str.length() == 1) {
            return str;
        } else {
            return str.charAt(str.length() - 1) + reverse(str.substring(0, str.length() - 1));
        }
    }
    public static void main(String[] args) {
        System.out.println("Program membalikkan string menggunakan teknik rekursif");
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan kata : ");
        String str = in.nextLine();
        in.close();
        System.out.println("Hasil : " + reverse(str));
    }
}
