import java.util.Scanner;

public class minMax {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Masukkan jumlah elemen array : ");
        int N = in.nextInt();
        int[][] arr = new int[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                System.out.print("Masukkan nilai M[" + i + "][" + j + "] : "); 
                arr[i][j] = in.nextInt();
            }
        }
        System.out.print("Masukkan nilai integer X : ");
        int X = in.nextInt();
        in.close();
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (arr[i][j] == X) {
                    System.out.println("True\nX ditemukan pada baris ke-" + (i + 1) + " kolom ke-" + (j + 1));
                }
            }
        }
        int min = arr[0][0];
        int max = arr[0][0];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (arr[i][j] < min) {
                    min = arr[i][j];
                }
                if (arr[i][j] > max) {
                    max = arr[i][j];
                }
            }
        }
        System.out.println("Nilai minimum : " + min);
        System.out.println("Nilai maksimum : " + max);
    }

}
