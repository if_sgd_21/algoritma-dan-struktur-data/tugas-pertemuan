import java.util.Scanner;

public class reverseNum {
    static int reverse(int n) {
        if (n < 10) {
            return n;
        } else {
            return (n % 10) * (int) Math.pow(10, (int) Math.log10(n)) + reverse(n / 10);
        }
    }
    public static void main(String[] args) {
        System.out.println("Program membalikkan angka menggunakan teknik rekursif");
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan angka : ");
        int n = in.nextInt();
        in.close();
        System.out.println("Hasil : " + reverse(n));
    }
}