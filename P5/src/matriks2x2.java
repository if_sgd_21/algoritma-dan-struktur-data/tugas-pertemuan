import java.util.Scanner;

public class matriks2x2 
{
    public static void main(String[] args) {
        //deklarasi matriks 2x2
        Scanner in =  new Scanner(System.in);
        int mtr[][] = new int[2][2];
        //input matriks
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                System.out.print("Masukkan matriks ke [" + i + "][" + j + "] : ");
                mtr[i][j] = in.nextInt();
            }
        }
        in.close();
        //menampilkan matriks
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                System.out.print(mtr[i][j] + " ");
            }
            System.out.print("\n");
        }
    }
}
