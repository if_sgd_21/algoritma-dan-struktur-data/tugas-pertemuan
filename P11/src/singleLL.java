import java.util.LinkedList;
import java.util.Scanner;
public class singleLL {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        LinkedList<String> list = new LinkedList<String>();
        System.out.println("===================================");
        System.out.println("SINGLE LINKED LIST");
        System.out.println("===================================");
        System.out.println("1. Add");
        System.out.println("2. Display (by sorting)");
        System.out.println("3. Search");
        System.out.println("4. Exit");
        System.out.println("===================================");
        System.out.print("Enter your choice: ");
        int choice = in.nextInt();
        while(choice != 4)
        {
            switch(choice)
            {
                case 1:
                    System.out.println("Enter the number of student's name to be added: ");
                    int n = in.nextInt();
                    for(int i = 0; i < n; i++)
                    {
                        System.out.print("Enter the name of student " + (i+1) + ": ");
                        String element = in.next();
                        list.add(element);
                    }
                    break;
                case 2:
                    System.out.println("===================================");
                    System.out.println("SORTED LIST");
                    System.out.println("===================================");
                    for(int i = 0; i < list.size(); i++)
                    {
                        for(int j = i + 1; j < list.size(); j++)
                        {
                            if(list.get(i).compareTo(list.get(j)) > 0)
                            {
                                String temp = list.get(i);
                                list.set(i, list.get(j));
                                list.set(j, temp);
                            }
                        }
                    }
                    for(int i = 0; i < list.size(); i++)
                    {
                        System.out.println(list.get(i));
                    }
                    break;
                case 3:
                    System.out.print("Enter the name to search: ");
                    String str2 = in.next();
                    int flag = 0;
                    for(int i = 0; i < list.size(); i++)
                    {
                        if(list.get(i).equals(str2))
                        {
                            flag = 1;
                            break;
                        }
                    }
                    if(flag == 1)
                    {
                        System.out.println("===================================");
                        System.out.println("SEARCH RESULT");
                        System.out.println("===================================");
                        System.out.println("The name is found in the list.");
                    }
                    else
                    {
                        System.out.println("===================================");
                        System.out.println("SEARCH RESULT");
                        System.out.println("===================================");
                        System.out.println("The name is not found in the list.");
                    }
                    break;
                default:
                    System.out.println("Invalid choice.");
                    break;
            }
            System.out.println("===================================");
            System.out.println("SINGLE LINKED LIST");
            System.out.println("===================================");
            System.out.println("1. Add");
            System.out.println("2. Display (by sorting)");
            System.out.println("3. Search");
            System.out.println("4. Exit");
            System.out.println("===================================");
            System.out.print("Enter your choice: ");
            choice = in.nextInt();
        }
    }
}