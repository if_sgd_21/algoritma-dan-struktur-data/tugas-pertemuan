import java.util.Scanner;

public class nilaiMhs {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan nilai Anda (dalam angka): ");
        int nilai = in.nextInt();
        if (nilai >= 85)
        {
            System.out.println("Nilai Anda adalah A.");
        }
        else if (nilai >= 75 && nilai <= 84)
        {
            System.out.println("Nilai Anda adalah AB.");
        }
        else if (nilai >= 65 && nilai <= 74)
        {
            System.out.println("Nilai Anda adalah B.");
        }
        else if (nilai >= 50 && nilai <= 64)
        {
            System.out.println("Nilai Anda adalah BC.");
        }
        else if (nilai >= 35 && nilai <= 49)
        {
            System.out.println("Nilai Anda adalah C.");
        }
        else if (nilai >= 20 && nilai <= 34)
        {
            System.out.println("Nilai Anda adalah D.");
        }
        else
        {
            System.out.println("Nilai Anda adalah E.");
        }
    }
}
