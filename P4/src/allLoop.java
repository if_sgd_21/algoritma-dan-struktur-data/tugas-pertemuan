public class allLoop {
    static void whileLoop()
    {
        System.out.println("While Loop :");
        int i = 0;
        while (i<10)
        {
            System.out.print(i + " ");
            i++;
        }
    }
    static void forLoop()
    {
        System.out.println("\nFor Loop :");
        for (int i=0; i<10; i++)
        {
            System.out.print(i + " ");
        }
    }
    static void doWhileLoop()
    {
        System.out.println("\nDo While Loop :");
        int i = 0;
        do
        {
            System.out.print(i + " ");
            i++;
        } while (i<10);
    }
    public static void main(String[] args) {
        whileLoop();
        forLoop();
        doWhileLoop();
    }
}
