public class NestedIf {
    public static void main(String[] args) {
        double uang = 10000;
        double barang = 15000;

        if (uang >= barang)
        {
            if (uang > barang * 6)
            {
                System.out.println("Anda Bisa Membeli 6 Kali Barang Saja");
            }
            else if (uang > barang * 5)
            {
                System.out.println("Anda Bisa Membeli 5 Kali Barang Saja");
            }
            else if (uang > barang * 4)
            {
                System.out.println("Anda Bisa Membeli 4 Kali Barang Saja");
            }
            else if (uang > barang * 3)
            {
                System.out.println("Anda Bisa Membeli 3 Kali Barang Saja");
            }
            else if (uang > barang * 2)
            {
                System.out.println("Anda Bisa Membeli 2 Kali Barang Saja");
            }
            else if (uang >= barang)
            {
                System.out.println("Anda Bisa Membeli 1 Kali Barang Saja");
            }
        } else {
            System.out.println("Uang Anda Tidak Cukup");
        }
        System.out.println("Alhamdulillah");
    }
}
