import java.util.Scanner;

public class loopPow {
    public static void main(String[] args) 
    {
        try (Scanner in = new Scanner(System.in)) {
            System.out.print("Masukkan nilai a: ");
            int a = in.nextInt();
            System.out.print("Masukkan nilai b: ");
            int b = in.nextInt();
            if (a>b)
            {
                System.out.println("Nilai a harus lebih kecil dari b");
                loopPow.main(args);
            }
            else if (b>1000)
            {
                System.out.println("Nilai b harus lebih kecil dari 1000");
                loopPow.main(args);
            }
            else
            {
                for (int i=a; i<b; i++)
                {
                    for (int j=(i+1); j<=b; j++)
                    {
                        System.out.println("Nilai a = "+i + " Nilai b = "+j);
                        double hasil = (Math.pow(i, 2)+Math.pow(j, 2)+1)/(i*j);
                        System.out.println("Hasil dari ("+i+"^2+"+j+"^2+1)/("+i+"*"+j+") = "+hasil);
                    }
                }
            }
        }
    }
}