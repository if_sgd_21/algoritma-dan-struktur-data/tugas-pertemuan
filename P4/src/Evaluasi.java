import java.util.Scanner;
public class Evaluasi {                         
    static void proseshasilUjian() {
        Scanner input = new Scanner(System.in);
        //Inisialisasi
        int jumlahLulus = 0;
        int jumlahGagall = 0;
        int counterSiswa = 1;
        int hasil;

        //while loop
        while(counterSiswa <= 8)
        {
            System.out.println("Masukkan hasil ujian (1 = lulus atau 2 = gagal): ");
            hasil = input.nextInt();

            //if else yang bersarang di while loop
            if(hasil == 1)
            {
                jumlahLulus = jumlahLulus + 1;
            }
            else
            {
                jumlahGagall = jumlahGagall + 1;
            }
            counterSiswa = counterSiswa + 1;
        }
        System.out.printf("Lulus: %d\nGagal : %d\n", jumlahLulus, jumlahGagall);

        if (jumlahLulus > 4)
        {
            System.out.println("Kursus telah berhasil");
        }
        else
        {
            System.out.println("Kursus tidak berhasil");
        }
    }
    public static void main(String[] args)
    {   
        proseshasilUjian();
    }
}
