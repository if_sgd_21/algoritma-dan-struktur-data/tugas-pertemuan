class Overload {
    void demo(int a) {
        System.out.printf("a: %d\n", a);
    }
    void demo(int a, int b)
    {
        System.out.printf("a: %d and b: %d\n", a, b);
    }
    double demo(double a)
    {
        System.out.printf("double a: %f\n", a);
        return a * a;
    }
}
class MethodOverloading
{
    public static void main(String[] args)
    {
        Overload obj = new Overload();
        double result;
        obj.demo(10);
        obj.demo(10, 20);
        result = obj.demo(5.5);
        System.out.println("O/P: " + result);
    }
}
