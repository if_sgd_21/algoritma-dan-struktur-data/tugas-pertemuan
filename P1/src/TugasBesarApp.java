import java.util.Scanner;

public class TugasBesarApp {
	public static void menu() {
		System.out.println("===================================\n"+
	 					   "       Peternakan Sapi Perah       \n"+		
						   "===================================");
		System.out.println("1. Data Hewan Ternak               ");
		System.out.println("2. Pemetaan Kandang                ");
		System.out.println("3. Jadwal Pengelolaan Peternakan   ");
		System.out.println("4. List Ternak Siap Perah          ");
		System.out.println("5. Stok Gudang                     ");
		System.out.println("6. Data Hasil Produksi & Penjualan ");
		System.out.println("0. Keluar                          ");
	}
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		do {
			menu();
			System.out.print("\nPilihan: ");
			int pil = in.nextInt();

			if (pil == 0) {
				System.exit(0);
			} else if (pil == 1) {
				DataHewanTernak.main(args);
			} else if (pil == 2) {
				//
			} else if (pil == 3) {
				//
			} else if (pil == 4) {
				//
			} else if (pil == 5) {
				//
			} else if (pil == 6) {
				//
			} else {
				System.out.print("Masukkan salah!");
			}	
		} while(true);	
	}
}