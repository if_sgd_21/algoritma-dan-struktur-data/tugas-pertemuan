import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("TUGAS KECIL");
        System.out.println("Silahkan pilih menu di bawah :");
        System.out.println("1. Program menampilkan hello world");
        System.out.println("2. Program menampilkan hello world dalam bahasa Indonesia");
        System.out.println("3. Keluar");
        System.out.println("Masukkan pilihan anda : ");
        int n = in.nextInt();
        while(n != 3) {
            switch (n) {
                case 1 :
                    System.out.println("Hello World");
                    break;
                case 2 :
                    System.out.println("Halo Dunia");
                    break;
                default :
                    System.out.println("Maaf, pilihan anda tidak tersedia");
                    break;
            }
            System.out.print("Masukkan pilihan anda : ");
            n = in.nextInt();
        }
    }
}
