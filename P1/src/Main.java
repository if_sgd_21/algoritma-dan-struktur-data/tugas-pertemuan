import java.util.*;
import java.io.*;

public class Main {
    void menu() {
    PrintStream out = null;
    try {
    out = new PrintStream(System.out, true, "UTF-8");
    } catch (UnsupportedEncodingException e) {
    e.printStackTrace();
    }
    System.out.println("""
    \033[0m=========================================
    \033[36m To Do List
    \033[0m=========================================
    \033[36m1. Tambah Antrian Pasien
    \033[36m2. Pemanggilan Pasien
    \033[36m3. Tampilkan Antrian Pasien
    \033[36m4. Tampilkan Antrian Pasien Berdasarkan Urutan Nama
    \033[36m5. keluar
    \033[0m=========================================
    """);
    }
    int nomor;
    String nama, Keperluan;
    Main next;
    static Scanner in = new Scanner(System.in);
    static Scanner str = new Scanner(System.in);

    public void input() {
        System.out.println();
        System.out.println("=========================================");
        System.out.print("Nomor Antrian     : ");
        nomor = in.nextInt();
        System.out.print("Nama pasien       : ");
        nama = str.nextLine();
        System.out.print("Keperluan         : ");
        Keperluan = str.nextLine();
        next = null;
    }

    public void read() {
        System.out.println("=========================================");
        System.out.println("Nomor Antrian   : " + nomor);
        System.out.println("Nama pasien     : " + nama);
        System.out.println("Keperluan       : " + Keperluan);
        System.out.println("=========================================");
        System.out.println();
    }

    public static void main(String[] args) {
        Main dp = new Main();
        int menu = 0;
        linked que = new linked();
        while (menu != 5) {
            dp.menu();
            System.out.print("Pilih Menu : ");
            menu = in.nextInt();

            if (menu == 1)
                que.enque();
            else if (menu == 2)
                que.deque();
            else if (menu == 3)
                que.view();
            else if (menu == 4) {
                que.sort();
            } else if (menu == 5) {
                System.out.println("Anda akan diarahkan ke Menu Utama");
                System.out.println("=========================================");
                Main.main(args);
            } else
                System.out.println("- Salah -");
    System.out.println("");
    }
    }
    }
    class linked {
        Main head, tail;
        int length;
        public linked() {
            head = null;
            tail = null;
            length = 0;
    }

    public void enque() {
        Main baru = new Main();
        baru.input();
        if (head == null)
            head = baru;
        else
            tail.next = baru;
        tail = baru;
        length++;
    }

    public void deque() {
        if (head == null) {
            System.out.println();
            System.out.println("=========================================");
            System.out.println("- Kosong -");
        } else {
            System.out.println();
            System.out.println("=========================================");
            System.out.println("Masuk ke Ruangan Untuk Antrian Nomor : " + head.nomor);
            head = head.next;
            length--;
        }
    }

    public void view() {
        if (head == null) {
            System.out.println();
            System.out.println("=========================================");
            System.out.println("- Kosong -");
        } else {
            System.out.println();
            System.out.println("=========================================");
            System.out.println("Antrian Pasien : ");
            Main temp = head;
            while (temp != null) {
                temp.read();
                temp = temp.next;
            }
        }
    }

    public void sort() {

        if (head == null) {
            System.out.println();
            System.out.println("=========================================");
            System.out.println("- Kosong -");
        }else{
            
            Main temp = head;
            Main[] array = new Main[length];
            int index = 0;
            while (temp != null) {
                array[index] = temp;
                index++;
                temp = temp.next;
            }

            // Sorting array based on name using bubble sort
            for (int i = 0; i < array.length - 1; i++) {
                for (int j = array.length - 1; j > i; j--) {
                    Main a = array[j];
                    Main b = array[j - 1];
                    if (a.nama.compareTo(b.nama) < 0) {
                        // Swap elements at indices j and j - 1
                        Main tempValue = array[j];
                        array[j] = array[j - 1];
                        array[j - 1] = tempValue;
                    }
                }
            }

            // Print the sorted list
            System.out.println();
            System.out.println("=========================================");
            System.out.println("Antrian Pasien Berdasarkan Urutan Nama : ");
            for (Main data : array) {
                data.read();
            }
        }
    }
}
