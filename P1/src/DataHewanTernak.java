import java.util.Scanner;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Calendar;  
import java.util.concurrent.TimeUnit;
import java.util.Date;

class NodeDataSapi {
	Scanner str = new Scanner(System.in);
	int nomorSapi;
	int umurSapi;
	String genderSapi;
	int masaLaktasi;
	NodeDataSapi next, prev;
	public NodeDataSapi(int nomorSapi, String tglLahir, String genderSapi) {
		this.nomorSapi = nomorSapi;
		this.umurSapi = (int) rentangHari(tglLahir)/365;
		this.genderSapi = genderSapi;
		if (genderSapi.equals("Betina")) {
			System.out.print("Apakah sedang menyusui?(y/t): ");
			String laktasi = str.nextLine();			
			if (laktasi.equals("y") || laktasi.equals("Y")) {
				System.out.print("Dari tanggal : ");
				masaLaktasi = (int) rentangHari(str.nextLine());
			} else {
				masaLaktasi = -1;
			}
		}
		System.out.println();
	}
	public long rentangHari(String tanggal) {
		try {
		    SimpleDateFormat formatTanggal = new SimpleDateFormat("dd/MM/yyyy");
			Calendar hariIni = Calendar.getInstance();
		    Date dateBefore = formatTanggal.parse(tanggal);
		    Date dateAfter = formatTanggal.parse(formatTanggal.format(hariIni.getTime()));

		    long bedaWaktu = Math.abs(dateAfter.getTime() - dateBefore.getTime());
		    long bedaHari = TimeUnit.DAYS.convert(bedaWaktu, TimeUnit.MILLISECONDS);
		    
		    return bedaHari;
		} catch(Exception e){
		    e.printStackTrace();
		}
		return -1;
	}
	public String getGender() {
		return genderSapi;
	}
	public void tampil() {
		System.out.println("Data Sapi#"+nomorSapi);
		System.out.println("Umur       : "+umurSapi+" tahun");
		System.out.println("Gender     : "+genderSapi);
		if (genderSapi.equals("Betina")) {
			if (masaLaktasi <= 200 & masaLaktasi >= 0) {
				System.out.print("Masa perah : "+masaLaktasi+" hari");
			}
		}
	}
	public void edit() {
		System.out.println("Kapan sapi mulai menyusui lagi?(hh/BB/tttt): ");
		this.masaLaktasi = (int) rentangHari(str.nextLine());
	}
}
class DoubleLinkedList {
	public NodeDataSapi head, tail;
	private NodeDataSapi currentNode;
	public DoubleLinkedList() {
		head = tail = currentNode = null;
	}
	public boolean isEmpty() {
		return head == null;
	}
	public void tambahData(int nomor) {
		System.out.println( "===================================\n"+
		 					"|         Tambah Data Sapi        |\n"+		
							"===================================");
		Scanner str = new Scanner(System.in);
		System.out.println("Data Sapi#"+nomor);
		System.out.print("Tanggal Lahir Sapi\n"+
						 "(hh/BB/tttt) : ");
		String tanggal = str.nextLine();
		System.out.print("Gender Sapi  : ");
		String genderSapi = str.nextLine();
		NodeDataSapi nodeBaru = new NodeDataSapi(nomor, tanggal, genderSapi);

		if (head == null) {
			head = nodeBaru;
			currentNode = nodeBaru;
		} else {
			tail.next = nodeBaru;
			nodeBaru.prev = tail;
		}
		tail = nodeBaru;
	}
	public void hapusData(NodeDataSapi temp) {
		if (head == tail) {
			head = tail = null;
		} else if (temp == head) {
			nextData();
			head = temp.next;
			temp.next.prev = null;
		} else if (temp == tail) {
			prevData();
			tail = temp.prev;
			temp.prev.next = null;
		} else {
			prevData();
			temp.prev.next = temp.next;
			temp.next.prev = temp.prev;
		}
	}
	public void tampilkanData(NodeDataSapi currentNode) {
		currentNode.tampil();
	}
	public void prevData()	{
		if (currentNode != null) {
			if (currentNode.prev != null) {
				currentNode = currentNode.prev;
			} else {
				currentNode = tail;
			}
		}
	}
	public void nextData()	{
		if (currentNode != null) {
			if (currentNode.next != null) {
				currentNode = currentNode.next;
			} else {
				currentNode = head;
			}	
		}
	}
	public void editData() {
		currentNode.edit();
	}
	public NodeDataSapi getCurrentNode() {
		return currentNode;
	}
	public int getLastNumber() {
		return tail.nomorSapi;
	}
}
public class DataHewanTernak {
	public static DoubleLinkedList sapi = new DoubleLinkedList();
	public static void menu() {
		System.out.println( "===================================\n"+
		 					"|        Data Hewan Ternak        |\n"+		
							"===================================");
		if (!sapi.isEmpty()) {
			sapi.tampilkanData(sapi.getCurrentNode());
		}
		System.out.println("\n-----------------------------------");
		System.out.print("1. Tambah data\t");
		if (!sapi.isEmpty()) {
			System.out.println("4. Data sebelumnya\n2. Hapus data\t5. Data selanjutnya");
			if (sapi.getCurrentNode().getGender().equals("Betina")) {
				System.out.print("3. Edit data");
			}
		}
		System.out.println("\n0. Kembali");
	}
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		int nomorSapi;
		if (sapi.isEmpty()) {
			nomorSapi = 1;
		} else {
			nomorSapi = sapi.getLastNumber()+1;
		} 
		while (true) {
			menu();
			System.out.print("\nPilihan: ");
			int pil = in.nextInt();

			if (pil == 0) {
				System.out.println("-----------------------------------");
				System.out.println("|     Beralih ke Menu Utama     |");
				System.out.println("-----------------------------------");
				TugasBesarApp.main(args);
			} else if (pil == 1) {
				sapi.tambahData(nomorSapi++);
			} else if (pil == 2) {
				sapi.hapusData(sapi.getCurrentNode());
			} else if (pil == 3) {
				sapi.editData();
			} else if (pil == 4) {
				sapi.prevData();
			} else if (pil == 5) {
				sapi.nextData();
			} else {
				System.out.print("Masukkan tidak diterima!");
			}	
		}	
	}
}