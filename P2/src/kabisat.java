import java.util.Scanner;

public class kabisat {
    public static void main(String[] args) {
        try (Scanner input = new Scanner(System.in)) {
            System.out.print("Masukkan Tahun : ");
            int tahun = input.nextInt();
            if (tahun % 4 == 0) {
                if (tahun % 100 == 0) {
                    if (tahun % 400 == 0) {
                        System.out.println("Tahun " + tahun + " adalah tahun kabisat");
                    } else {
                        System.out.println("Tahun " + tahun + " bukan tahun kabisat");
                    }
                } else {
                    System.out.println("Tahun " + tahun + " adalah tahun kabisat");
                }
            } else {
                System.out.println("Tahun " + tahun + " bukan tahun kabisat");
            }
        }
    }
}
