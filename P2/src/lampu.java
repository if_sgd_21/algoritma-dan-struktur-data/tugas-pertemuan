import java.util.Scanner;

public class lampu {
    public static void main(String[] args) {
        try (Scanner input = new Scanner(System.in)) {
            //Mengidentifikasi lampu lalu lintas
            System.out.print("Masukkan warna lampu (GUNAKAN HURUF KAPITAL): ");
            String warna = input.next();
            if (warna.equals("MERAH")) {
                System.out.println("Berhenti!");
            } else if (warna.equals("KUNING")) {
                System.out.println("Hati-hati!");
            } else if (warna.equals("HIJAU")) {
                System.out.println("Jalan!");
            } else {
                System.out.println("Warna salah");
            }
        }
    }
}
