import java.util.Scanner;

public class suhu {
    public static void main(String[] args) {
        try (Scanner input = new Scanner(System.in)) {
            System.out.print("Masukkan suhu dalam Celcius : ");
            int t = input.nextInt();
            if (t < 0)
            {
                System.out.println("Suhu " + t + " Celcius adalah zat padat");
            }
            else if (t >= 0 && t <= 100)
            {
                System.out.println("Suhu " + t + " Celcius adalah zat cair");
            }
            else if (t > 100)
            {
                System.out.println("Suhu " + t + " Celcius adalah zat gas");
            }
        }
    }
}
