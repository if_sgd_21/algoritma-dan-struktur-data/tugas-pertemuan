import java.util.Scanner;

public class perakaran {
    public static void main(String[] args) 
    {
        //  Buatlah Program Java untuk untuk menghitung akar-akar persamaan kuadrat dengan rumus:
        //  D=B*B-4*A*C
        //  X1= -B+akar(D)/2*A
        //  X2= -B-akar(D)/2*A
        //  D=Diskriminan
        //  X1=akar pertama
        //  X2=akar kedua
        //  A,B,C=koefisien
        //  D=Diskriminan
        System.out.println("Menghitung akar-akar persamaan kuadrat");
        try (Scanner input = new Scanner(System.in)) {
            System.out.print("Masukkan nilai A : ");
            int a = input.nextInt();
            System.out.print("Masukkan nilai B : ");
            int b = input.nextInt();
            System.out.print("Masukkan nilai C : ");
            int c = input.nextInt();
            int d = b * b - 4 * a * c;
            double x1 = (-b + Math.sqrt(d)) / (2 * a);
            double x2 = (-b - Math.sqrt(d)) / (2 * a);
            if (d < 0)
            {
                System.out.println("Akar-akar persamaan kuadrat adalah imajiner");
            }
            else if (d == 0)
            {
                d = -b / (2*a);
                System.out.println("Akar-akar persamaan kuadrat adalah kembar");
                System.out.println("Akar-akar persamaan kuadrat adalah : " + d);
            }
            else {
                System.out.println("Akar-akar persamaan kuadrat adalah x1 : " + x1 + " dan x2 : " + x2);
            }
        }
    }
}
