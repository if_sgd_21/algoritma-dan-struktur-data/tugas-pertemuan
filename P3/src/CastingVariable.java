import java.util.Scanner;

public class CastingVariable {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in))
        {
            System.out.print("Masukkan alas : ");
            int var_alas = in.nextInt();
            System.out.print("Masukkan tinggi : ");
            int var_tinggi = in.nextInt();
            double rumus_luas;
            rumus_luas = (double)(var_alas * var_tinggi)/2;
            System.out.println("Luas segitiga : " + rumus_luas);
        }
    }
}
