package Queue;
public class QueueLinkedList<T> {

    public static void main(String[] args) {
        QueueLinkedList<Integer> q = new QueueLinkedList<Integer>();
        q.enqueue(1);
        q.enqueue(2);
        q.enqueue(3);
        System.out.println(q.toString());
    }
    private class Node{
        T item;
        Node next;

        Node(T t){
            item = t;
            next = null;
        }

        public String toString()  {
            return  item.toString();
        }
    }
    private Node first, last;
    public void enqueue(T t){
        Node oldLast = last;
        last = new Node(t);
        
    }
}
