package Queue;
import java.util.Scanner;
public class Queue {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        CQueue q = new CQueue();
        System.out.print("Masukkan jumlah Mahasiswa yang akan ikut bimbingan : ");
        q.queue = new String[in.nextInt()];
        int pilih;
        do {
            System.out.println("1. Enqueue");
            System.out.println("2. Dequeue");
            System.out.println("3. Show");
            System.out.println("4. Check Storage");
            System.out.println("5. Exit");
            System.out.print("Pilih : ");
            pilih = in.nextInt();
            switch (pilih) {
                
                case 1:
                    System.out.print("Masukkan jumlah Mahasiswa yang ikut Bimbingan: ");
                    int n = in.nextInt();
                    for (int i = 0; i < n; i++) {
                        System.out.print("Masukkan Nama Mahasiswa ke-" + (i + 1) + ": ");
                        q.enqueue(in.next());
                    }
                    break;
                case 2:
                    System.out.println("Masukkan jumlah Mahasiswa yang sudah selesai : ");
                    n = in.nextInt();
                    for (int i = 0; i < n; i++) {
                        q.dequeue();
                    }
                    break;
                case 3:
                    q.show();
                    break;
                case 4:
                    System.out.println("Sisa antrian yang tersedia : " + (q.queue.length - q.getSize()));
                    break;
            }
        } while (pilih != 5);
    }
}
