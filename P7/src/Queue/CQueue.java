package Queue;

public class CQueue {
    int n;
    String queue[] = new String[n]; // deklarasi array
    int front;
    int rear;
    int size;

    public void enqueue(String data)
    {
        if(!isFull())
        {
            queue[rear] = data;
            rear = (rear + 1) % queue.length;
            size++;
        }
        else
        {
            System.out.println("Antrian penuh");
        }
    }

    public void show()
    {
        System.out.println("Data Mahasiswa : ");
        System.out.print("[");
        for (int i = 0; i < size; i++)
        {
            if(i < size-1)
            {
                System.out.print(queue[(front+i)%queue.length] + ", "); // <-- line 1
            } else if (i == size-1) {
                System.out.print(queue[(front+i)%queue.length]);
            }
        }
        System.out.print("]\n");
    }

    public String dequeue()
    {
        if(!isEmpty())
        {
            String data = queue[front];
            front = (front + 1) % queue.length;
            size--;
            return data;
        }
        else
        {
            System.out.println("Antrian kosong");
            return null;
        }
    }
    public int getSize()
    {
        return size;
    }
    public boolean isEmpty()
    {
        return getSize() == 0;
    }
    public boolean isFull()
    {
        return getSize() == queue.length;
    }
}