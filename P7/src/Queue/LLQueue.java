package Queue;

import java.util.Scanner;
public class LLQueue<T> {
	public class Node {
		T item;
		Node next;
		Node(T data) {
			item = data;
			next = head;
		}
	}
	private Node head, tail, temp;
	LLQueue() {
		head = tail = null;
	}
	boolean isEmpty() {
		return (tail == null && head == null);
	}
	void enqueue(T data) {
		temp = new Node(data);
        if (head == null) {
            head = temp;
            temp.next = head;
            head = temp;

            tail = temp;
            head.next = tail;
            tail.next = head;
        } else {
        	tail.next = temp;
        	tail = tail.next;
        }
		System.out.println(data + " telah masuk ke antrian.");
	}
	void dequeue() {
		if (!isEmpty()) {
			T deq = head.item;
			if (head == tail) {
	            head = null;
	            tail = null;
	        } else {
	        	tail.next = head.next;
	        	head = tail.next;
	        }
			System.out.println(deq + " telah keluar dari antrian.");	        
		} else {
			System.out.println("Antrian kosong!");
		}
	}
	void tampil() {
		if (!isEmpty()) {
        	if (head == tail) {
            	System.out.print(head.item+"<h><t>");
	        } else {
		        StringBuffer sb = new StringBuffer();
		        temp = head;
		        while (temp != tail) {
		            sb.append(temp.item);
		            if (temp == head) {
		            	sb.append("<h>");
		            }
		            sb.append("-");
		            temp = temp.next;
		        }
		 
		        sb.append(temp.item+"<t>");
		        System.out.print(sb.toString());	
	        }      
        } else {
        	System.out.println("Antrian kosong!");
        }
	}
	void clear() {
		if (!isEmpty()) {
			head = tail = null;
			System.out.println("Antrian telah dihapus.");
		} else {
			System.out.println("Antrian kosong!");
		}
	}
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		LLQueue<String> antri = new LLQueue<String>();
		int pil = 0;
		
		System.out.print("======================\n");
		System.out.print("LinkedList Queue\n");
		System.out.print("======================\n");
		System.out.print("1. Enqueue          \n");
		System.out.print("2. Dequeue          \n");
		System.out.print("3. Tampilkan Queue  \n");
		System.out.print("4. Clear Queue      \n");
		System.out.print("5. Selesai          \n");
		do {
			System.out.print("\nPilihan: ");

			pil = Integer.parseInt(in.nextLine());
			System.out.print("\n----------------------------\n");

			if (pil == 1) {
				System.out.print("Masukkan Data: ");
				String data = in.nextLine();
				antri.enqueue(data);
			} else if (pil == 2) {
				antri.dequeue();
			} else if (pil == 3) {
				antri.tampil();
			} else if (pil == 4) {
				antri.clear();
			}			
			System.out.print("\n----------------------------\n");
		} while (pil != 5);
		System.out.println("Program Selesai.");
	}
}

