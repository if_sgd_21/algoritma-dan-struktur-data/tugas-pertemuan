import java.util.Scanner;

public class Equals {
    public static void main(String[] args) {
        String name = "Dayen";
        String name2 = "Dayen";
        String name3 = new String("Dayen");
        //primitive types
        //int, char, float, double, short, byte, long
        if (name3.equals("Dayen"))
        {
            System.out.println("True");
        } else {
            System.out.println("False");
        }
    }
}
