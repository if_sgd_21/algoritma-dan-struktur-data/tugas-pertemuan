import java.util.TreeSet;
import java.util.ArrayList;
import java.util.Scanner;

public class TugasP10 {
    void treeSet()
    {
        Scanner in = new Scanner(System.in);
        TreeSet<String> nama = new TreeSet<String>();
        System.out.print("Masukkan jumlah nama yang akan diinput : ");
        int n = in.nextInt();
        for (int i = 0; i < n; i++)
        {
            System.out.print("Masukkan nama ke " + (i+1) + " : ");
            nama.add(in.next());
        }
        System.out.println("Tree Set: " + nama); //otomatis tersorting berdasarkan abjad
    }
    void arrayList()
    {
        Scanner in = new Scanner(System.in);
        ArrayList<String> nama = new ArrayList<String>();
        System.out.print("Masukkan jumlah nama yang akan diinput : ");
        int n = in.nextInt();
        for (int i = 0; i < n; i++)
        {
            System.out.print("Masukkan nama ke " + (i+1) + " : ");
            nama.add(in.next());
        }
        System.out.println("ArrayList: " + nama); //otomatis tersorting berdasarkan abjad
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        TugasP10 p = new TugasP10();
        System.out.println("TUGAS PERTEMUAN 10 : TREESET DAN ARRAYLIST");
        System.out.println("Silahkan pilih menu berikut ini :");
        System.out.println("1. Input nama dengan TreeSet\n2. Input nama dengan ArrayList\n3. exit");
        System.out.print("Masukkan pilihan anda : ");
        int x = in.nextInt();
        while(x != 3)
        {
            switch(x) {
                case 1 :
                    p.treeSet();
                    break;
                case 2 :
                    p.arrayList();
                    break;
                case 3 :
                    System.out.println("Terima kasih telah menggunakan program ini");
                    break;
                default :
                    System.out.println("Pilihan tidak tersedia");
            }
            System.out.println("Apakah anda ingin mengulang Program? (y/n)");
            String y = in.next();
            if (y.equals("y"))
            {
                System.out.println("Silahkan pilih menu berikut ini :");
                System.out.println("1. Input nama dengan TreeSet\n2. Input nama dengan ArrayList\n3. exit");
                System.out.print("Masukkan pilihan anda : ");
                x = in.nextInt();
            }
            else
            {
                x = 3;
            }
        }
        in.close();
    }
}