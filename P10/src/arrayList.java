import java.util.ArrayList;
import java.util.TreeSet;
import java.util.Scanner;

public class arrayList {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        TreeSet<String> arr = new TreeSet<String>();
        System.out.println("Program menambahkan arrayList tanpa batas");
        System.out.println("Input STOP untuk mengakhiri penambahan");
        while (!arr.contains("STOP"))
        {
            arr.add(in.next());
        }
        arr.remove("STOP");
        System.out.println(arr);
    }
}
