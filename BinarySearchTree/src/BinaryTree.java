public class BinaryTree {
    public Node root;

    public void add(Node node) {
        if (root == null) {
            root = node;
        } else {
            insert(root, node);
        }
    }

    public void insert(Node parent, Node node) {
        if (node.getValue() < parent.getValue()) {
            if (parent.left == null) {
                parent.left = node;
            } else { 
                insert(parent.left, node);
            }
        } else {
            if (parent.right == null) {
                parent.right = node;
            } else {
                insert(parent.right, node);
            }
        }
    }

    public static boolean search(Node root, int value) {
        if (root == null) {
            return false;
        } else if (root.getValue() == value) {
            return true;
        } else if (value < root.getValue()) {
            return search(root.left, value);
        } else {
            return search(root.right, value);
        }
    }
    

}
