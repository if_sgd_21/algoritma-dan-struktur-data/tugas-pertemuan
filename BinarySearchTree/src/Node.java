public class Node {
    private int value;
    public Node left;
    public Node right;

    public Node(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
