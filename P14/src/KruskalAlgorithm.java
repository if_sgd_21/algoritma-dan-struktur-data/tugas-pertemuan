import java.util.InputMismatchException;
import java.util.Scanner;

public class KruskalAlgorithm {
    
        private int number_of_nodes;
        private int[] parent;
    
        public void kruskal(int adjacency_matrix[][]) {
            number_of_nodes = adjacency_matrix[1].length - 1;
            int cost[][] = new int[number_of_nodes + 1][number_of_nodes + 1];
            for (int i = 1; i <= number_of_nodes; i++)
                for (int j = 1; j <= number_of_nodes; j++)
                    cost[i][j] = adjacency_matrix[i][j];
            int minimum_cost = 0;
            parent = new int[number_of_nodes + 1];
            for (int node = 1; node <= number_of_nodes; node++)
                parent[node] = node;
            int edge_count = 0;
            while (edge_count < number_of_nodes - 1) {
                int min = Integer.MAX_VALUE;
                int a = 0;
                int b = 0;
                for (int i = 1; i <= number_of_nodes; i++) {
                    for (int j = 1; j <= number_of_nodes; j++) {
                        if (find(i) != find(j) && cost[i][j] < min) {
                            min = cost[i][j];
                            a = i;
                            b = j;
                        }
                    }
                }
                union1(a, b);
                System.out.println("Edge " + edge_count++ + ":(" + a + "," + b + ") cost:" + min);
                minimum_cost += min;
            }
            System.out.println("Minimum cost = " + minimum_cost);
        }
    
        public int find(int i) {
            while (parent[i] != i)
                i = parent[i];
            return i;
        }
    
        public void union1(int i, int j) {
            int a = find(i);
            int b = find(j);
            parent[a] = b;
        }
    
// KruskalAlgorithm.java
// Enter the number of vertices 6 
// Enter the Weighted Matrix for the graph 
// 0 6 8 6 0 0 
// 6 0 0 5 10 0 
// 8 0 0 7 5 3
// 6 5 7 0 0 0
// 0 10 5 0 0 3
// 0 0 3 0 3 0
// Kirimkan gambar graph asal dan graph hasil penelusuran yang dibuat ke lms/eknows folder Tugas 14 dalam format word.
// Dengan nama file: Tugas 14 Praktikum AlStrukdata Nim-nama-kelas
// Kirimkan juga file source code program yang dibuat ke lms/eknows folder Tugas 14 dalam format zip.
    public static void main(String[] args) {
        int number_of_nodes, source;
        Scanner scanner = null;
        try {
            System.out.println("Enter the number of vertices ");
            scanner = new Scanner(System.in);
            number_of_nodes = scanner.nextInt();
            int adjacency_matrix[][] = new int[number_of_nodes + 1][number_of_nodes + 1];
            System.out.println("Enter the Weighted Matrix for the graph ");
            for (int i = 1; i <= number_of_nodes; i++)
                for (int j = 1; j <= number_of_nodes; j++)
                    adjacency_matrix[i][j] = scanner.nextInt();
            System.out.println("The Transitive Closure of the Graph");
            KruskalAlgorithm kruskalAlgorithm = new KruskalAlgorithm();
            kruskalAlgorithm.kruskal(adjacency_matrix);
        } catch (InputMismatchException inputMismatch) {
            System.out.println("Wrong Input format");
        }
        scanner.close();
    }

}
